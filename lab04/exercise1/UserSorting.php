<html>

<head>
    <title>User sorting</title>
    <link rel="stylesheet" href="template.css">
</head>

<?php
function user_sort($a, $b)
{
    // smarts is all-important, so sort it first
    if ($b == 'smarts') {
        return 1;
    } else if ($a == 'smarts') {
        return -1;
    }

    return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
}

$values = array(
    'name' => 'Buzz Lightyear',
    'email_address' => 'buzz@starcommand.gal',
    'age' => 32,
    'smarts' => 'some'
);

if (isset($_POST['submitted'])) {
    $sort_type =  $_POST['sort_type'];
    if ($sort_type == 'usort' || $sort_type == 'uksort' || $sort_type == 'uasort') {
        $sort_type($values, 'user_sort');
    } else {
        $sort_type($values);
    }
}
?>

<body>
    <h1>User sorting</h1>
    <div class="form-section">
        <form action="UserSorting.php" method="post">
            <table>
                <tr>
                    <td>
                        <input type="radio" name="sort_type" value="sort" checked="checked" /> Standard sort <br />
                        <input type="radio" name="sort_type" value="rsort" /> Reverse sort <br />
                    </td>
                    <td>
                        <input type="radio" name="sort_type" value="usort" /> User-defined sort <br />
                        <input type="radio" name="sort_type" value="ksort" /> Key sort <br />
                    </td>
                    <td>
                        <input type="radio" name="sort_type" value="krsort" /> Reverse key sort <br />
                        <input type="radio" name="sort_type" value="uksort" /> User-defined key sort <br />
                    </td>
                    <td>
                        <input type="radio" name="sort_type" value="asort" /> Value sort <br />
                        <input type="radio" name="sort_type" value="arsort" /> Reverse value sort <br />
                        <input type="radio" name="sort_type" value="uasort" /> User-defined value sort <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="submit-btn-cell">
                        <input type="submit" value="Sort" name="submitted" />
                    </td>
                </tr>
            </table>
        </form>
    </div> <!-- end of div.form-section -->

    <div class="display-lists-section">
        <div class="one-list">
            <p>
                Values unsorted:
            </p>
            <ul>
                <li><b>name</b>: Buzz Lightyear</li>
                <li><b>email_address</b>: buzz@starcommand.gal</li>
                <li><b>age</b>: 32</li>
                <li><b>smarts</b>: some</li>
            </ul>
        </div> <!-- end of div.one-list -->
        <div class="one-list">
            <p>
                Values <?= isset($_POST['submitted']) ? ("sorted by " . $_POST['sort_type']) : "unsorted"; ?>:
            </p>
            <ul>
                <?php
                foreach ($values as $key => $value) {
                    echo "<li><b>$key</b>: $value</li>";
                }
                ?>
            </ul>
        </div> <!-- end of div.one-list -->
    </div> <!-- end of div.display-lists-section -->
</body>

</html>