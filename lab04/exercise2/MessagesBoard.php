<html>

<head>
    <title>Messages Board</title>
    <link rel="stylesheet" href="template.css">
</head>

<?php
if (isset($_POST['submit'])) {
    $error = 0;

    // check if username or msg is blank
    if (!isset($_POST['username']) || trim($_POST['username']) == "") {
        $error = 1;
    }

    if (!isset($_POST['msg']) || trim($_POST['msg']) == "") {
        $error = 1;
    }

    if ($error == 1) {
        print("Username and Message content can not be blank!<br>");
    } else {
        // upload file to folder "posts"
        if (isset($_FILES['fileToUpload'])) {
            $filePath = './posts/post_' . $_SERVER['REQUEST_TIME'] . '.' . pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
            move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $filePath);
        }

        // create message to write into file
        $msgContent = "";
        if (isset($_POST['userIsAnonymous'])) {
            $msgContent = "Someone";
        } else {
            $msgContent = $_POST["username"];
        }
        $msgContent = $msgContent . " says \"" . $_POST["msg"] . "\"";

        // write into file
        $fp = @fopen('./posts/' . 'post_' . $_SERVER['REQUEST_TIME'] . '.txt', "w");
        fwrite($fp, $msgContent);
    }
}
?>

<body>
    <h1>Messages Board</h1>
    <div class="display-msg-section">
        <?php
        $path = './posts/';
        $fileName = scandir($path);
        $numberOfFile = count($fileName);

        for ($i = 2; $i < $numberOfFile; $i++) {
            print "<div class = \"one-msg\">";

            // print the image if the message has image
            if (!strpos($fileName[$i], '.txt')) {
                $fpath = $path . $fileName[$i];
                print "<img src='$fpath' width=25%>";
                $i++;
            }

            // print the text
            $fpath = $path . $fileName[$i];
            $fp = @fopen($fpath, "r");
            while (!feof($fp)) {
                print fgets($fp);
            }

            print "</div>"; // end of one-msg
        }
        ?>
    </div> <!-- end of .display-msg-section -->

    <h1>Post a message</h1>
    <div class="form-section">
        <form enctype="multipart/form-data" method="post">
            <p>
                Username:<br>
                <input type="text" name="username"><br>
                <input type="checkbox" name="userIsAnonymous"> Post as anonymous
            </p>
            <p>
                Message content:<br>
                <textarea name="msg" rows="5" cols="50"></textarea>
            </p>
            <p>
                Attaching imagine (optional):<br>
                <input type="file" name="fileToUpload">
            </p>
            <p align="center">
                <input type="submit" value="Post message" name="submit">
            </p>
        </form>
    </div> <!-- end of .form-section -->
</body>

</html>