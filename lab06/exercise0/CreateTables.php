<?php
require_once('DatabaseConfig.php');

if (!$connect) {
    die("Cannot connect");
} else {
    $table_name1 = 'Businesses';
    $SQLcmd1 = "CREATE TABLE $table_name1 (
            BusinessID INT AUTO_INCREMENT PRIMARY KEY,
            Name VARCHAR(50),
            Address VARCHAR(50),
            City VARCHAR(50),
            Telephone VARCHAR(50),
            URL VARCHAR(50))";

    $table_name2 = 'Categories';
    $SQLcmd2 = "CREATE TABLE $table_name2 (
            CategoryID INT AUTO_INCREMENT PRIMARY KEY,
            Title VARCHAR(50),
            Description VARCHAR(50))";

    $table_name3 = 'Biz_Categories';
    $SQLcmd3 = "CREATE TABLE $table_name3 (
        BusinessID INT,
        CategoryID INT)";

    $SQLcmd4 = "ALTER TABLE Biz_Categories
        ADD FOREIGN KEY (BusinessID) REFERENCES Businesses (BusinessID),
        ADD FOREIGN KEY (CategoryID) REFERENCES Categories (CategoryID)";

    $SQLcmds = [$SQLcmd1, $SQLcmd2, $SQLcmd3, $SQLcmd4];
    $table_names = [$table_name1, $table_name2, $table_name3];

    mysqli_select_db($connect, $mydb);

    for ($i = 0; $i < 3; $i++) {
        if (mysqli_query($connect, $SQLcmds[$i])) {
            print '<font size="4" color="blue">Created Table';
            print "<i>" . $table_names[$i] . "</i> in database<i>$mydb</i><br></font>";
            print "<br>SQLcmd=" . $SQLcmds[$i] . "<br>";
        } else {
            die("Table Create Creation Failed SQLcmd=" . $SQLcmds[$i] . "<br>");
        }
    }

    if (mysqli_query($connect, $SQLcmd4)) {
        print '<font size="4" color="blue">ADDED FOREIGN KEY';
    } else {
        die("Add foreign key failed SQLcmd=" . $SQLcmd4);
    }
}

mysqli_close($connect);
