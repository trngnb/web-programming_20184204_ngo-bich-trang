<html>

<head>
    <title>Category Administration</title>
</head>

<?php
require_once('../exercise0/DatabaseConfig.php');

$results_id = '';

if (isset($_POST['submit'])) {
    if (empty($_POST['cateID']) || empty($_POST['title'])  || empty($_POST['description'])) {
        $msg = "You need to fill all the fields before submit";
    } else {
        mysqli_select_db($connect, $mydb);
        $table_name = 'Categories';
        $cateID = $_POST['cateID'];
        $title = $_POST['title'];
        $description = $_POST['description'];
        
        $sqlCmd = "INSERT INTO $table_name 
                VALUES('$cateID', '$title', '$description')";

        if (mysqli_query($connect, $sqlCmd)) {
            $msg = "Insert category successfully";
        } else {
            $msg = "Insert category failed";
        }

        $select_query = "SELECT * FROM $table_name";
        $results_id = mysqli_query($connect, $select_query);
    }
}
?>

<body>
    <h1>Category Administration</h1>
    <form method="post">
        <table>
            <tr>
                <th bgcolor="#eeeeee">Cat ID</th>
                <th bgcolor="#eeeeee">Title</th>
                <th bgcolor="#eeeeee">Description</th>
            </tr>
            <?php
            // display any records fetched from the database
            // plus an input line for a new category
            if ($results_id) {
                while ($row = mysqli_fetch_row($results_id)) {
                    print '<tr>';
                    foreach ($row as $field) {
                        print "<td>$field</td>";
                    }
                    print '</tr>';
                }
            }
            ?>
            <tr>
                <td><input type="text" name="cateID" size="15" maxlength="10" /></td>
                <td><input type="text" name="title" size="40" maxlength="128" /></td>
                <td><input type="text" name="description" size="45" maxlength="255" /></td>
            </tr>
        </table>
        <input type="submit" name="submit" value="Add Category"></input>
    </form>
    <?php echo empty($msg) ? " " : $msg ?>

</body>

</html>