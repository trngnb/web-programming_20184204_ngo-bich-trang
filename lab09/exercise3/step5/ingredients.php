<?php
// set name of XML file
$file = "ingredients.xml";

// load file
$xml = simplexml_load_file($file) or die("Unable to load XML file!");

// get all the <desc> elements and print
echo '<b>All items:</b><br>';
foreach ($xml->xpath('//desc') as $desc) {
    echo "$desc<br>";
}

echo '<br><br>';

// get all the <desc> elements and print
echo '<b>All items that has quantity > 1:</b><br>';
foreach ($xml->xpath('//item[quantity > 1]/desc') as $desc) {
    echo "$desc<br>";
}

?>
