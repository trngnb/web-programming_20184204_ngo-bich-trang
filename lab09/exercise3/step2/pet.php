<?php

function readPet($xml)
{
    // access XML data
    echo "Name: " . $xml->name . "<br>";
    echo "Age: " . $xml->age . "<br>";
    echo "Species: " . $xml->species . "<br>";
    echo "Parents: " . $xml->parents->mother . " and " . $xml->parents->father . "<br>";
}

function writePet($file, $xml, $name, $age, $species, $mother, $father)
{
    // modify XML data
    $xml->name = $name;
    $xml->age = $age;
    $xml->species = $species;
    $xml->parents->mother = $mother;
    $xml->parents->father = $father;

    // write new data to file
    file_put_contents($file, $xml->asXML());
}

function restoreOldPet($file, $xml)
{
    writePet($file, $xml, "Polly Parrot", 3, "parrot", "Pia Parrot", "Peter Parrot");
}

// set name of XML file
$file = "pet.xml";

// load file
$xml = simplexml_load_file($file) or die("Unable to load XML file!");

// read XML data
echo 'Before modify:<br>';
readPet($xml);

// modify XML data
writePet($file, $xml, "Sammy Snail", 4, "snail", "Sue Snail", "Sid Snail");

// read new XML data
echo '<br>After modify:<br>';
readPet($xml);

// restore old data: the parrot
restoreOldPet($file, $xml);

?>