<?php
session_start();
require_once("db_config.php");

$error = "";
$sql = "SELECT * FROM products";
$result = mysqli_query($db, $sql);
if (!$result) {
    $error = "DB is empty";
}

if (!isset($_SESSION["cart"])) {
    $_SESSION["cart"] = array();
}

// add product to cart
if (isset($_GET['add']) && (!empty($_GET['add'] || $_GET['add'] == 0))) {
    $sql2 = "SELECT * FROM products WHERE ID = " . $_GET['add'];
    $result2 = mysqli_query($db, $sql2);
    $row2 = mysqli_fetch_array($result2);

    array_push($_SESSION["cart"], array(
        'productID' => $row2["ID"],
        'name' => $row2["Name"],
        'price' => $row2["Price"],
        'image' => $row2["Image"],
        'deliverable' => 'Download'
    ));
}

?>

<html>

<head>
    <title>The store</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <div class="search-box-wrapper">
            <a href="homepage.php">Home</a>
            <form action="product_details.php" method="get">
                <select name="productID" id="" class="search-input" style="width:85%;height:30px">
                    <option value="" disabled selected>--Search for a product--</option>
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            print "<option value=" . $row['ID'] . " >" . $row['Name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" value="Search" class="search-btn bigger-btn">
            </form>
            <a href="view_shopping_cart.php">View your cart</a>
        </div>
    </header>

    <div class="wrapper">
        <?php echo $error; ?>


        <h1>Welcome to The store</h1>
        <hr>

        <!-- <div class="product-list">
            <form action="" method="post">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="software-title">Software Title</th>
                            <th>Deliverable</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $product_in_cart = $_SESSION["cart"];
                        $total_price = 0;
                        $qty = count($product_in_cart);

                        if ($qty == 0) {
                            echo '<td colspan="4">Your cart is empty</td>';
                        } else {
                            foreach ($product_in_cart as $key => $value) {
                                $total_price += $product_in_cart[$key]["price"];
                        ?>
                                <tr>
                                    <td class="img-wrapper">
                                        <img src="<?php echo $product_in_cart[$key]["image"]; ?>" alt="">
                                    </td>
                                    <td>
                                        <p><?php echo $product_in_cart[$key]["name"]; ?></p>
                                        <a href="" style="margin-right: 10px;">Edit</a>
                                        <a href="?remove=<?php echo $key; ?>">Remove</a>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $product_in_cart[$key]["deliverable"]; ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $product_in_cart[$key]["price"]; ?>$
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="2" class="btn-wrapper" style="text-align: right;">
                                <input type="submit" value="Check out" name="submit">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div> -->

        <div class="product-list">
            <!-- foreach -->
            <?php
            $result = mysqli_query($db, $sql);
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
            ?>
                    <div class="one-product">
                        <form action="" method="post">
                            <div class="product-name"><?php echo $row['Name'] ?></div>
                            <div class="img-wrapper">
                                <img src="<?php echo $row["Image"]; ?>" alt="">
                            </div>
                            <a href="product_details.php?productID=<?php echo $row["ID"]; ?>">View details</a>
                            <a href="?add=<?php echo $row["ID"]; ?>">Add to cart</a>
                        </form>
                    </div>

            <?php
                }
            }
            ?>


        </div>
    </div>
</body>

</html>