<?php
session_start();
require_once("db_config.php");

$error = "";
$msg = "";

$sql = "SELECT ID, Name FROM products";
$result = mysqli_query($db, $sql);

if (!$result) {
    $error = "DB is empty";
}

$productID = $_GET['productID'];
$sql2 = "SELECT * FROM products WHERE ID = $productID";
$result2 = mysqli_query($db, $sql2);
if (!$result2) {
    $error = "product ID not found!";
} else {
    $row2 = mysqli_fetch_array($result2);
}

// add product to cart
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['deliverable'])) {
        $deliverable = 'Download';
    } else {
        $deliverable = 'Not download';
    }

    array_push($_SESSION["cart"], array(
        'productID' => $row2["ID"],
        'name' => $row2["Name"],
        'price' => $row2["Price"],
        'image' => $row2["Image"],
        'deliverable' => $deliverable
    ));

    $msg = 'Added product to cart!';
}
?>

<html>

<head>
    <title><?php echo $row2['Name']; ?></title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<header>
        <div class="search-box-wrapper">
            <a href="homepage.php">Home</a>
            <form action="product_details.php" method="get">
                <select name="productID" id="" class="search-input" style="width:85%;height:30px">
                    <option value="" disabled selected>--Search for a product--</option>
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            print "<option value=" . $row['ID'] . " >" . $row['Name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" value="Search" class="search-btn bigger-btn">
            </form>
            <a href="view_shopping_cart.php">View your cart</a>
        </div>
    </header>

    <div class="wrapper">
        <?php echo $error; ?>

        <div class="product-details">
            <h1>Product Details</h1>
            <hr>

            <table class="product-details-table">
                <tr>
                    <th>Product name</th>
                    <td><?php echo $row2['Name']; ?></td>
                    <td class="img-wrapper" rowspan="5">
                        <img src="<?php echo $row2["Image"]; ?>" alt="">
                        <br>
                        <a href="">View Product Description</a>
                    </td>
                </tr>
                <tr>
                    <th>Publisher</th>
                    <td><a href="<?php echo $row2["PublisherURL"]; ?>"><?php echo $row2["Publisher"]; ?></a></td>
                </tr>
                <tr>
                    <th>SKU</th>
                    <td><?php echo $row2["SKU"]; ?></td>
                </tr>
                <tr>
                    <th>Platform</th>
                    <td><?php echo $row2["Platform"]; ?></td>
                </tr>
            </table>
        </div>

        <div class="deliver-and-price">
            <form action="" method="post">
                <table class="deliver-and-price-table">
                    <tr>
                        <td></td>
                        <th>Deliverable</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="deliverable" id=""></td>
                        <th>Download</th>
                        <td>Choose this option if you wish to download the software over the Internet.</td>
                        <td><?php echo $row2['Price']; ?>$</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="add-to-cart-wrapper">
                            <span class="msg-wrapper" style="margin-right: 10px;">
                                <?php
                                echo $msg;
                                $msg = "";
                                ?>
                            </span>
                            <input type="submit" value="Add to cart" class="add-to-cart-btn bigger-btn"">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

</body>

</html>