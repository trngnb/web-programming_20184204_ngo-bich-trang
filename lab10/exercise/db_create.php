<?php
require_once("db_config.php");

// create table Products
$sql = "CREATE TABLE IF NOT EXISTS Products (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            Name VARCHAR(50),
            Price FLOAT,
            Publisher VARCHAR(50),
            PublisherURL VARCHAR(100),
            SKU VARCHAR(50),
            Platform VARCHAR(50),
            Image VARCHAR(50)
            );";

if (mysqli_query($db, $sql)) {
    print '<font size="4" color="blue">Created Table';
    print "<i>Products</i> in database<i>$mydb</i><br></font>";
    print "<br>SQLcmd=" . $sql . "<br><br>";
} else {
    die("Table Create Creation Failed SQLcmd=" . $sql . "<br>");
}

// insert into table Products
$sql = "INSERT INTO `products`(`ID`, `Name`, `Price`, `Publisher`, `PublisherURL`, `SKU`, `Platform`, `Image`) VALUES
(1,'MSDNAA Program',99,'Microsoft','https://www.microsoft.com/','0000','Windows','images/1.png'),
(2,'Access 2000',49,'Microsoft','https://www.microsoft.com/','0000','Windows','images/2.png'),
(3,'Office 365',99,'Microsoft','https://www.microsoft.com/','0000','Windows','images/3.png'),
(4,'SQL server 2012 Express Edition',29,'Microsoft','https://www.microsoft.com/','0000','Windows','images/4.png');";

if (mysqli_query($db, $sql)) {
    print '<font size="4" color="blue">Inserted into Table';
    print "<i>Products</i> in database<i>$mydb</i><br></font>";
    print "<br>SQLcmd=" . $sql . "<br>";
} else {
    die("Table Create Creation Failed SQLcmd=" . $sql . "<br>");
}
?>