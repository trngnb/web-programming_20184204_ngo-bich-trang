<html>

<head>
    <title>String Processor</title>
    <link rel="stylesheet" href="template.css">
    <style type="text/css">
        table td,
        th {
            padding-left: 30px;
        }

        table th {
            text-align: left;
        }

        table td.submit {
            text-align: right;
        }
    </style>
</head>

<?php
$result = "";

if (isset($_POST['submit'])) {
    switch ($_POST['func']) {
        case "strlen":
            $result = strlen($_POST['inputString']);
            break;
        case "strtolower":
            $result = strtolower($_POST['inputString']);
            break;
        case "trim":
            $result = trim($_POST['inputString']);
            break;
        case "strtoupper":
            $result = strtoupper($_POST['inputString']);
            break;
    }
}
?>

<body>
    <div>
        <h2>Simple String Processor</h2>
        <form method="POST">
            <table>
                <tr>
                    <th colspan="2">Enter a string:</th>
                </tr>
                <tr>
                    <td colspan="2"><input type="text" name="inputString"></td>
                </tr>
                <tr>
                    <td><input type="radio" name="func" value="strlen" checked> strlen</td>
                    <td><input type="radio" name="func" value="strtoupper"> strtoupper</td>
                </tr>
                <tr>
                    <td><input type="radio" name="func" value="trim"> trim</td>
                    <td><input type="radio" name="func" value="strtolower"> strtolower</td>
                </tr>
                <tr>
                    <td colspan="2" class="submit"><input type="submit" name="submit"></td>
                </tr>
                <tr>
                    <th colspan="2">Result:</th>
                </tr>
                <tr>
                    <td colspan="2"><input type="text" readonly="readonly" name="result" value="<?php echo $result ?>"></td>
                </tr>
            </table>
        </form>
    </div>
</body>

</html>