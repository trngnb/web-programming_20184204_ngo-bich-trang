<html>

<head>
    <title>Simple Calculator</title>
    <link rel="stylesheet" href="template.css">
    <style>
        table {
            text-align: right;
            line-height: 20px;
        }

        table td.inputbox {
            text-align: left;
        }

        table td.submit {
            padding-right: 35px;
        }

        table td.op {
            width: 5px;
            text-align: left;
        }
    </style>
</head>
<?php
$result = 0;

if (isset($_POST['submit'])) {
    if (!is_numeric($_POST['x1']) && !is_numeric($_POST['x2'])) {
        print("You need to enter digit for first and second numbers first!");
        $result = "ERROR";
    } else {
        switch ($_POST['op']) {
            case '+':
                $result = $_POST['x1'] + $_POST['x2'];
                break;
            case '-':
                $result = $_POST['x1'] - $_POST['x2'];
                break;
            case '*':
                $result = $_POST['x1'] * $_POST['x2'];
                break;
            case '/':
                if ($_POST['x2'] == 0) {
                    print("The second number can not be zero!");
                    $result = "ERROR";
                    break;
                }
                $result = $_POST['x1'] / $_POST['x2'];
                break;
            default:
                print("You need to select an operation first!");
        }
    }
}
?>

<body>
    <div>
        <h2>A Simple Calculator</h2>
        <form method="POST">
            <table>
                <tr>
                    <th>First number</th>
                    <td colspan="4" class="inputbox"><input type="text" name="x1"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="op"><input type="radio" name="op" value="+" checked> +</td>
                    <td class="op"><input type="radio" name="op" value="-"> -</td>
                    <td class="op"><input type="radio" name="op" value="*"> *</td>
                    <td class="op"><input type="radio" name="op" value="/"> /</td>
                </tr>
                <tr>
                    <th>Second number</th>
                    <td colspan="4" class="inputbox"><input type="text" name="x2"></td>
                </tr>
                <tr>
                    <td colspan="5" class="submit"><input type="submit" name="submit" value="Calculate"></td>
                </tr>
                <tr>
                    <th>Result</th>
                    <td colspan="4" class="inputbox"><input readonly="readonly" name="result" value="<?php echo $result ?>"></td>
                </tr>
            </table>
        </form>
    </div>
</body>

</html>