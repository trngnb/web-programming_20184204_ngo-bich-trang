<?php

$products = array('note', 'notebook', 'novel', 'book', 'pen', 'ruler', 'eraser', 'napkin', 'newspaper');
$len = count($products);
$suggestion = "";

$q = $_GET["q"];

if (strlen($q) > 0) {
    $suggestion = "";

    for ($i = 0; $i < $len; $i++) {
        if (stristr($products[$i], $q)) {
            $suggestion = $suggestion . '<a href="">' . $products[$i] . '</a><br>';
        }
    }
}

if ($suggestion == "") {
    $response = "product name not found";
} else {
    $response = $suggestion;
}

echo $suggestion;
