<!DOCTYPE html>
<html>

<head>
    <title>Validate inputs</title>
    <style>
        .grapper {
            margin: auto;
            width: 350px;
        }

        span {
            display: inline-block;
            width: 80px;
        }
    </style>
</head>

<body>
    <div class="grapper">
        <h1>Validation Form</h1>
        <form id="myForm">
            <p>
                <span>Username: </span>
                <input type="text" name="username" id="username" />
            </p>
            <p>
                <span>Email: </span>
                <input type="text" name="email" id="email" />
            </p>
            <p>
                <span>Telephone: </span>
                <input type="text" name="telephone" id="telephone" />
            </p>
            <input type="submit" value="Submit" onclick="validateInput()">
        </form>
    </div>

    <script>
        var error = "";

        function validateInput() {
            error = "";

            valideUsername();
            validatePhonenumber();
            validateEmail();

            if (error === "") {
                alert("Input valid!");
            } else {
                alert(error);
            }
        }

        function valideUsername() {
            var field = document.getElementById("username");

            if (field.value == null || field.value == '') {
                error += "Invalid username\n";
            }
        }

        function validatePhonenumber() {
            var regex = new RegExp("(03|07|08|09|01[2|6|8|9])+([0-9]{8})");
            var field = document.getElementById("telephone");

            if (!regex.test(field.value)) {
                error += "Invalid phone number\n";
                return false;
            }
        }

        function validateEmail() {
            var field = document.getElementById("email");
            var regex = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");

            if (!regex.test(field.value)) {
                error += "Invalid email\n";
                return false;
            }
        }
    </script>
</body>

</html>