<html>

<head>
    <title>Date Time Processing</title>
    <link rel="stylesheet" href="DateTimeProcessing_template.css">
</head>

<body>
    <h1>Book An Appointment</h1>
    <div class="input-box">
        <p>Enter your name and select date and time for the appointment</p>
        <form method="POST">
            <table>
                <tr>
                    <!-- name row -->
                    <th>Your name:</th>
                    <td><input type="text" name="userName"></td>
                </tr> <!-- end of name row -->
                <tr>
                    <!-- date row -->
                    <th>Date:</th>
                    <td>
                        <select name="day">
                            <option value="0" selected="selected">-dd-</option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                printf("<option value=\"%d\">%02d</option>", $i, $i);
                            }
                            ?>
                        </select>
                        <select name="month">
                            <option value="0" selected="selected">-mm-</option>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                printf("<option value=\"%d\">%02d</option>", $i, $i);
                            }
                            ?>
                        </select>
                        <select name="year">
                            <option value="0" selected="selected">-yyyy-</option>
                            <?php
                            $currentYear = date('Y');

                            // can choose the year from the current one to the next 10 years
                            for ($i = 0; $i < 10; $i++) { // should have used GLOBAL var 
                                $year = $currentYear + $i;
                                print("<option value=\"$year\">$year</option>");
                            }
                            ?>
                        </select>
                    </td>
                </tr> <!-- end of date row -->
                <tr>
                    <!-- time row -->
                    <th>Time:</th>
                    <td>
                        <select name="hour">
                            <option value="-1" selected="selected">-hh-</option>
                            <?php
                            for ($i = 0; $i <= 23; $i++) {
                                printf("<option value=\"%d\">%02d</option>", $i, $i);
                            }
                            ?>
                        </select>
                        <select name="minute">
                            <option value="-1" selected="selected">-mm-</option>
                            <?php
                            for ($i = 0; $i <= 59; $i++) {
                                printf("<option value=\"%d\">%02d</option>", $i, $i);
                            }
                            ?>
                        </select>
                        <select name="second">
                            <option value="-1" selected="selected">-ss-</option>
                            <?php
                            for ($i = 0; $i <= 59; $i++) {
                                printf("<option value=\"%d\">%02d</option>", $i, $i);
                            }
                            ?>
                        </select>
                    </td>
                </tr> <!-- end of time row -->
                <tr>
                    <!-- btn row -->
                    <td colspan="2" class="btn-td">
                        <input type="reset">
                        <input type="submit" name="submit">
                    </td>
                </tr> <!-- end of btn row -->
            </table>
        </form>
    </div> <!-- end of div.input-box -->

    <div class="msg-box">
        <?php
        require("CheckValidDateTimeFunction.php");
        require("PrintDateTimeFunction.php");

        if (isset($_POST['submit'])) {
            $day = $_POST['day'];
            $month = $_POST['month'];
            $year = $_POST['year'];
            $hour = $_POST['hour'];
            $minute = $_POST['minute'];
            $second = $_POST['second'];

            if (checkValidDate($day, $month, $year) && checkValidTime($hour, $minute, $second)) {
                // inform input succeed
                print("<h3>Hi " . $_POST['userName'] . ",<br>");
                printf("You have successfully booked an appointment at %02d:%02d:%02d ", $hour, $minute, $second);
                printf("on %02d/%02d/%d !</h3><br>", $day, $month, $year);

                // print info about the date
                print("More information about the date:<br>");
                print("In 12-hour system, the time and date is: ");
                printTimeIn12h($hour, $minute, $second);
                printf(", %02d/%02d/%d.<br>", $day, $month, $year);
                printDateInfo($day, $month, $year);
            } else {
                print("<h3>Invalid date or time! Please re-enter the date and time.<h3>");
            }
        }
        ?>
    </div><!-- end of div.msg-box -->
</body>

</html>