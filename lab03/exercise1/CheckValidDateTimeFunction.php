<?php

// return true if the time is valid, false if otherwise
// a time date has:
// - hour from 0 ~ 23
// - minute and second from 0 ~ 59
function checkValidTime(int $hour, int $minute, int $second)
{
    // check hour
    if ($hour < 0 || $hour > 23) {
        return false;
    }

    // check minute
    if ($minute < 0 || $minute > 59) {
        return false;
    }

    // check minute
    if ($second < 0 || $second > 59) {
        return false;
    }

    return true;
}

// return true if the date is valid, false if otherwise
// a valid date has:
// - year cannot be negative or 0
// - month from 1 ~ 12
// - day:
//   + month == 2 -> if leap year then the day can be from 1 ~ 29
//                   otherwise then the day can be from 1 ~ 28
//   + month == 4, 6, 9, 11 -> day from 1 ~ 30
//   + other case -> day from 1 ~ 31
function checkValidDate(int $day, int $month, int $year)
{
    // could have just used checkdate()

    // check year
    if ($year < 1) {
        return false;
    }

    // check month
    if ($month < 1 || $month > 12) {
        return false;
    }

    // check day
    if ($day < 1 || $day > 31) {
        return false;
    } elseif ($month == 2) {
        if ($day == 29 && !isLeapYear($year)) {
            return false;
        } else if ($day > 29) {
            return false;
        }
    } elseif ($month == 4 || $month == 6 || $month == 9 || $month == 11) {
        if ($day == 31) {
            return false;
        }
    }

    return true;
}

// return true if the year is a leap year, false if otherwise
// a leap year is:
// - the year can be divided by 4
// - the year can be divided by 4 and 100 -> can be divided by 400
// - but other case that can be divided by 100 is not a leap year
function isLeapYear(int $year)
{
    // could have used date("L", ...)

    if ($year % 400 == 0) {
        return true;
    }

    if ($year % 100 == 0) {
        return false;
    }

    if ($year % 4 == 0) {
        return true;
    }

    return false;
}

// for exercise 2

require_once("../exercise2b/StrToDateFunction.php");

function checkValidStringDate(string $str)
{
    // check valid date string: needs to be in valid format: "dd-mm-yyyy"
    $date = strToDate($str);

    if ($date === NULL) {
        return false;
    }

    // check valid date
    if (!checkValidDate($date[0], $date[1], $date[2])) {
        return false;
    }

    return true;
}

?>
