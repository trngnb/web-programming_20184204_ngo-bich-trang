<?php

require_once("CheckValidDateTimeFunction.php");

// print time in 12h system
function printTimeIn12h(int $hour, int $minute, int $second)
{
    $period = " AM";

    if ($hour > 12) {
        $hour = $hour - 12;
        $period = " PM";
    }

    printf("%02d:%02d:%02d %s", $hour, $minute, $second, $period);
}

// print info about the date:
// - whether the year is a leap year
// - number of day of the month
function printDateInfo(int $day, int $month, int $year)
{
    if (!checkValidDate($day, $month, $year)) {
        print("null");
        return;
    }

    // print info about the year: is leap year or not
    $isLeapYear = false;

    if (isLeapYear($year)) {
        $isLeapYear = true;
        print("The year $year is a leap year.<br>");
    }

    // print info about the month: what the number of day in the month is
    $numOfDay = 31;

    switch ($month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            $numOfDay = 30;
            break;
        default: // case 2:
            if (!$isLeapYear) {
                $numOfDay = 28;
            } else {
                $numOfDay = 29;
            }
    }

    print("The month $month has $numOfDay days.<br>");
}

// for exercise 2

function printDateInLetter(string $strDate)
{
    if (!checkValidStringDate($strDate)) {
        print("null");
        return;
    }

    print(date("l, F jS Y", strtotime($strDate)));
}

?>
