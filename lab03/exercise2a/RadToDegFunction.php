<html>

<head>
    <title>Unit of Angle Converter</title>
    <style>
        body {
            background-color: #f2f2f2;
            font-family: Segoe UI light;
            font-size: 17px;
            text-align: center;
            line-height: 50px;
        }

        h1 {
            margin-top: 80px;
        }

        div.wrapper {
            margin: auto;
            margin-top: 10px;
            width: 275px;
            height: 150px;
            padding: 25px 30px 5px 30px;
            background-color: white;
            border-radius: 50px;
            box-shadow: 0px 5px 20px rgba(149, 157, 165, 0.2);
        }

        div.wrapper span {
            padding-right: 10px;
        }
    </style>
</head>

<body>
    <h1>Unit of Angle Converter</h1>
    <div class="wrapper">
        <form method="POST">
            <span>Value:</span>
            <input type="text" name="value">
            <br>
            <input type="radio" name="unit" value="rad" checked><span>Radians</span>
            <input type="radio" name="unit" value="deg"><span>Degrees</span>
            <br>
            <input type="submit" name="submit">
        </form>
    </div> <!-- end of div.wrapper -->

    <?php
    function convertAngleTo(string $value, string $unit)
    {
        // check valid input
        if (!is_numeric($value)) {
            return NULL;
        }

        // convert
        if ($unit == "rad") {
            return rad2deg($value);
        } elseif ($unit == "deg") {
            return deg2rad($value);
        } else {
            return NULL;
        }
    }

    if (isset($_POST['submit'])) {
        $value = $_POST['value'];
        $unit = $_POST['unit'];

        $result = convertAngleTo($value, $unit);
        $resultUnit = "";

        if ($result === NULL) {
            print("Invalid Input! The input must be numeric.<br>");
        } else {
            // convert
            if ($unit == "rad") {
                $resultUnit = "deg";
            } else {
                $resultUnit = "rad";
            }

            // print
            print("<h3>$value $unit = $result $resultUnit</h3>");
        }
    }
    ?>

</body>

</html>