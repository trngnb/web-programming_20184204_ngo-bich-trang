<html>

<head>
    <title>Birthday stuff</title>
    <link rel="stylesheet" href="DTF-template.css">
</head>

<body>
    <h1>Birthday Calculator</h1>
    <div class="input-box">
        <form method="post">
            <table>
                <tr>
                    <th colspan="2" class="userNo">The 1st person:</th>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td><input type="text" name="user1"></td>
                </tr>
                <tr>
                    <th>Birthday:</th>
                    <td><input type="text" name="bday1"></td>
                </tr>
                <tr>
                    <th colspan="2" class="userNo">The 2nd person:</th>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td><input type="text" name="user2"></td>
                </tr>
                <tr>
                    <th>Birthday:</th>
                    <td><input type="text" name="bday2"></td>
                </tr>
                <tr>
                    <td colspan="2" class="btn"><input type="submit" name="submit"></td>
                </tr>
            </table>
        </form>
    </div> <!-- end of div.input-box -->

    <div class="msg-box">
        <?php
        require_once("../exercise1/CheckValidDateTimeFunction.php");
        require_once("StrToDateFunction.php");
        require_once("../exercise1/PrintDateTimeFunction.php");

        function printAge(string $userName, string $bday) {
            $today = date("d-m-Y");
            $diff = date_diff(date_create($bday), date_create($today));
            printf(". $userName is $diff->y years old.</h3>");
        }

        if (isset($_POST['submit'])) {
            $user1 = $_POST['user1'];
            $user2 = $_POST['user2'];
            $bday1 = $_POST['bday1'];
            $bday2 = $_POST['bday2'];

            // check valid dates
            if (!checkValidStringDate($bday1) || !checkValidStringDate($bday2)) {
                print("<h4>Invalid date! Birthday needs to be in \"dd-mm-yy\" format.</h4>");
            } else {
                // print the dates and ages
                print("<h4>$user1's birthday is on ");
                printDateInLetter($bday1);
                printAge($user1, $bday1);
                print("<h4>$user2's birthday is on ");
                printDateInLetter($bday2);
                printAge($user2, $bday2);         

                // calculate the difference in days between two dates
                // C1: not gonna be 100% correct
                // $diff = abs(strtotime($bday1) - strtotime($bday2)); 
                // $dayDiff = floor($diff / (60 * 60 * 24));           
                // C2:
                $dateDiff = (new DateTime($bday1))->diff(new DateTime($bday2));
                print("The difference in days between two dates is $dateDiff->days.<br>");

                // print difference in years 
                // C1:  not gonna be 100% correct, and is less by 1
                // $yDiff = floor($diff / (60 * 60 * 24 * 365));   
                // print("The difference in years between the two people is $yDiff.<br>");
                // C2: less by 1
                // print("The difference in years between two dates is $dateDiff->y.<br>"); 
                // C3:
                $year1 = date("Y", strtotime($bday1));
                $year2 = date("Y", strtotime($bday2));
                $yearDiff = abs($year1 - $year2);
                print("The difference in years between the two people is $yearDiff.<br>");
            }
        }
        ?>
    </div> <!-- end of div.msg-box -->
</body>

</html>