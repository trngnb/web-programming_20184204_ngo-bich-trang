<?php

function strToDate(string $str)
{
    $date = [];
    $date = explode("-", $str);

    // return NULL if wrong format: "dd-mm-yyyy"
    $numOfElem = count($date);
    if ($numOfElem != 3) {
        return NULL;
    }

    // return NULL if the elem is not string of int
    for ($i = 0; $i < 3; $i++) {
        if (!ctype_digit($date[$i])) {
            return NULL;
        }
    }

    // return NULL if the date is invalid, eg: february cant have the 29th day in a non-leap year
    if (!checkValidDate($date[0], $date[1], $date[2])) {
        return NULL;
    }
    
    return $date;
}
