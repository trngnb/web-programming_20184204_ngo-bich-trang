<html>

<head>
    <title>Validate date</title>
    <link rel="stylesheet" href="template.css">
    <style>
        .wrapper {
            margin: auto;
            margin-top: 10px;
            width: 250px;
            height: 135px;
            padding: 30px 10px 0px 10px;
            background-color: white;
            border-radius: 50px;
            box-shadow: 0px 5px 20px rgba(149, 157, 165, 0.2);
        }
    </style>
</head>

<body>
    <h1>Validate date</h1>

    <div class="wrapper">
        <form method="post">
            Enter date (mm/dd/yyyy):
            <br><br>
            <input type="text" size="15" maxlength="10" name="date">
            <br><br>
            <input type="submit" value="Enter" name="submit">
        </form>
    </div>

    <div class="msg-box">
        <?php
        $two = '[[:digit:]]{2}';
        $month = '(0[1-9]|1[0-2])';
        $day = '(0[1-9]|[1-2][[:digit:]]|3[0-1])';
        $year = "2[[:digit:]]$two";

        if (isset($_POST['submit'])) {
            $date = $_POST['date'];

            if (preg_match("/^($month)\/($day)\/($year)$/", $date)) {
                list($month, $day, $year) = preg_split('/\//', $date);

                if (checkdate($month, $day, $year)) {
                    print '<b>Valid date</b><br>';
                    print "month = $month, day = $day, year = $year";
                } else {
                    print "<b> Invalid date value: $date </b>";
                }
            } else {
                print "<b> Invalid date format: $date </b>";
            }
        }
        ?>
    </div>

</body>

</html>