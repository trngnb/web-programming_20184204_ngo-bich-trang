<html>

<head>
    <title>Product Information Results</title>
</head>

<body>
    <font size="5" color="blue"> Happy Harry's Hardware Product Information </font><br>

    <form method="post">
        <br>Enter product code (Use AB## format):<br>
        <input type="text" size="6" name="code">
        <br> Please enter description:<br>
        <input type="text" size="50" name="description">
        <br><br>
        <input type="reset" value="Reset">
        <input type="submit" name="submit" value="Send product code">
    </form>

    <?php
    $products = array(
        'AB01' => '25-Pound Sledgehammer',
        'AB02' => 'Extra Strong Nails',
        'AB03' => 'Super Adjustable Wrench',
        'AB04' => '3-Speed Electric Screwdriver'
    );

    if (isset($_POST['submit'])) {
        $code = $_POST['code'];
        $description = $_POST['description'];

        if (preg_match('/boat|plane/', $description)) {
            print 'Sorry, we do not sell boats or planes anymore';
        } elseif (preg_match('/^AB/', $code)) {
            if (isset($products["$code"])) {
                print "Code $code Description: $products[$code]";
            } else {
                print 'Sorry, product code not found';
            }
        } else {
            print 'Sorry, all our product codes start with "AB"';
        }
    }
    ?>

</body>

</html>