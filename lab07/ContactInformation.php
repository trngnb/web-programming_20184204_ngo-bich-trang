<html>

<head>
    <title>Contact Information</title>
    <link rel="stylesheet" href="template.css">
</head>

<body>
    <h1>Contact Information</h1>

    <div class="input-box">
        <form method="POST">
            <table>
                <tr>
                    <th>Email address:</th>
                    <td><input type="text" name="email"></td>
                </tr>
                <tr>
                    <th>URL address:</th>
                    <td><input type="text" name="url"></td>
                </tr>
                <tr>
                    <th>Phone number:</th>
                    <td><input type="text" name="phoneNumber"></td>
                </tr>
                <tr>
                    <td colspan="2" class="btn-td">
                        <input type="reset" value="Reset">
                        <span style="margin-left: 20px;"></span>
                        <input type="submit" value="Submit" name="submit">
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="msg-box">
        <?php
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $url = $_POST['url'];
            $phoneNumber = $_POST['phoneNumber'];
            $error = 0;

            // should use filter_var instead
            if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)) {
                print 'Invalid email<br>';
                $error = 1;
            }

            // should use filter_var instead
            if (!preg_match("/^(http(s?):\/\/)([a-z0-9\-]+\.)+[a-z]{2,4}(\.[a-z]{2,4})*(\/[^ ]+)*$/", $url)) {
                print 'Invalid URL<br>';
                $error = 1;
            }

            if (!preg_match('/^0[0-9]{9}$/', $phoneNumber)) {
                print 'Invalid phone number<br>';
                $error = 1;
            }

            if ($error == 0) {
                print 'Submit contact information successfully';
            }
        }
        ?>
    </div>
</body>

</html>